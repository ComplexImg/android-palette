package by.bsuir.kurs;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.loader.PalettesLoader;
import by.bsuir.kurs.loader.UserLoader;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.User;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Response> {

    EditText login, pass;
    Button registerBtn, loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.loginText);
        pass = (EditText) findViewById(R.id.passText);
        registerBtn = (Button) findViewById(R.id.registerBtn);
        loginBtn = (Button) findViewById(R.id.loginBrn);


        SharedDB db = SharedDB.getObj(MainActivity.this);


        //temp


        db.updateUser( new User());


        db.setMemoryDBFields();


        User user = MemoryDB.getUser();

        if (user.getUsername() != null){
            login.setText(user.getUsername());
            pass.setText(user.getPassword());
            loadData();
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }


    private void loadData(){

        User user = new User();
        user.setUsername(login.getText().toString());
        user.setPassword(pass.getText().toString());
        MemoryDB.setUser(user);

        Toast.makeText(MainActivity.this, "Loading data...", Toast.LENGTH_SHORT).show();

        getLoaderManager().initLoader(0, Bundle.EMPTY, this);


    }


    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                return new PalettesLoader(this);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        int id = loader.getId();
        if (id == 0) {
            if (data.allRight()){
                List<Palette> palettes = data.getTypedAnswer();

                Toast.makeText(MainActivity.this, "Ok!", Toast.LENGTH_SHORT).show();

            }else {
                // неверный юзер
                Toast.makeText(MainActivity.this, "NO DATA!", Toast.LENGTH_SHORT).show();
            }


        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
        // Do nothing
    }

}
